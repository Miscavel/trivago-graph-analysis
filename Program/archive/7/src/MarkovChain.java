import com.sun.org.apache.xpath.internal.operations.Mult;
import org.apache.avro.generic.GenericData;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.math3.analysis.function.Multiply;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.server.namenode.FSImageFormatProtobuf;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

public class MarkovChain
{
    private static List<String> content;
    private static Hashtable<String, Node> dictionary = new Hashtable<String, Node>();
    private static String step_delimiter = "~";
    private static String item_delimiter = ";";
    private static String comma_ex_quote_delimiter = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";

    public static class SessionActionMapper extends Mapper<Object, Text, Text, Text>
    {
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            String line = value.toString();
            String data[] = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
            String ref, city;

            ref = "[" + data[5] + "]";
            city = data[7];

            if (data[4].equals("clickout item"))
            {
                String hotel_arr[] = data[10].split("\\|");
                String hotel_id = String.join("#", hotel_arr);

                String price_arr[] = data[11].split("\\|");
                String price_id = String.join("#", price_arr);

                ref += "[" + hotel_id + "]";
                ref += "[" + price_id + "]";
            }

            context.write(new Text(data[1] + ";" + city), new Text(data[3] + "," + data[4] + " " + ref + "->"));
        }
    }

    public static class SessionActionReducer extends Reducer<Text, Text, Text, Text>
    {
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            String value = "";
            for (Text val : values)
            {
                value += val.toString();
            }
            context.write(key, new Text(value));
        }
    }

    public static class PriceMapper extends Mapper<Object, Text, Text, Text>
    {
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            String line = value.toString();
            String data[] = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
            String hotel_list, price_list;
            String hotel_arr[], price_arr[];

            try
            {
                if (data[4].equals("clickout item"))
                {
                    hotel_list = data[10];
                    price_list = data[11];

                    hotel_arr = hotel_list.split("\\|");
                    price_arr = price_list.split("\\|");

                    for (int i = 0; i < hotel_arr.length; i++)
                    {
                        context.write(new Text(hotel_arr[i]), new Text(price_arr[i] + ";"));
                    }
                }
            }
            catch (Exception e)
            {
                System.out.println(line);
                System.out.println(data.length);
                System.out.println(e);
            }
        }
    }

    public static class PriceReducer extends Reducer<Text, Text, Text, Text>
    {
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            String value = "";
            for (Text val : values)
            {
                value += val.toString();
            }
            context.write(key, new Text(value));
        }
    }

    public static class ImpressionMapper extends Mapper<Object, Text, Text, Text>
    {
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            String line = value.toString();
            String data[] = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
            String hotel_list;
            String hotel_arr[];
            String clicked_hotel;

            try
            {
                if (data[4].equals("clickout item"))
                {
                    hotel_list = data[10];
                    clicked_hotel = data[5];
                    hotel_arr = hotel_list.split("\\|");

                    boolean exist = false;
                    for (int i = 0; i < hotel_arr.length; i++)
                    {
                        if (hotel_arr[i].equals(clicked_hotel))
                        {
                            exist = true;
                            break;
                        }
                    }

                    if (!exist)
                    {
                        context.write(new Text(data[0] + ";" + data[1] + ";" + data[3]), new Text(clicked_hotel + ";"));
                    }
                }
            }
            catch (Exception e)
            {
                System.out.println(line);
                System.out.println(data.length);
                System.out.println(e);
            }
        }
    }

    public static class ImpressionReducer extends Reducer<Text, Text, Text, Text>
    {
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            String value = "";
            for (Text val : values)
            {
                value += val.toString();
            }
            context.write(key, new Text(value));
        }
    }

    public static void MapNodeRecursive(Node node, List<String> visitedNodes, Node result, int count, int depth_threshold, Double preceding_probability)
    {
        if (visitedNodes.indexOf(node.id) != -1 || count > depth_threshold)
        {
            return;
        }
        else
        {
            visitedNodes.add(node.id);
        }

        for (int i = 0; i < node.items.size(); i++)
        {
            Double probability = preceding_probability * node.probability.get(i);

            if (node.items.get(i).id.contains(","))
            {
                continue;
            }
            result.Register(null, node.items.get(i).id, probability);
        }

        for (int i = 0; i < node.items.size(); i++)
        {
            if (node.items.get(i).id.contains(","))
            {
                continue;
            }

            if (!(count + 1 > depth_threshold))
            {
                MapNodeRecursive(node.items.get(i), visitedNodes, result, count + 1, depth_threshold, preceding_probability); //Static Probability
            }
        }
    }

    public static void CalculateAccuracy(String sourceFile, String resultFile) throws IOException
    {
        Double markov_length = 0.0, impressions_length = 0.0;

        BufferedReader source = new BufferedReader(new FileReader(sourceFile));
        FileWriter writer = new FileWriter(resultFile);
        BufferedWriter buffer = new BufferedWriter(writer);

        String source_line;
        String source_id, prediction_id, region_id, session_id;
        String[] source_data;
        String nodeData;
        int left_bracket, right_bracket;
        Double count = 0.0, correct = 0.0, none = 0.0, previous = 0.0, incorrect = 0.0;
        List<String> checkedNodes = new ArrayList<>();
        List<Integer> track_record = new ArrayList<>();
        for (int i = 0; i < 26; i++)
        {
            track_record.add(0);
        }

        int depth = 0;

        while ((source_line = source.readLine()) != null)
        {
            source_data = source_line.split("\\|");
            session_id = source_data[0].split(";")[0];
            region_id = source_data[0].split(";")[1];
            source_data = source_data[1].split("->");
            source_id = source_data[source_data.length - 1];
            left_bracket = source_id.indexOf("[");
            right_bracket = source_id.indexOf("]");
            source_id = "[" + source_id.substring(left_bracket + 1, right_bracket) + "]";

            int answer_index = -1;
            Node temp;
            prediction_id = "none";

            Node result = new Node(session_id);

            //Impressions Start
            nodeData = source_data[source_data.length - 1];

            nodeData = nodeData.substring(nodeData.indexOf("]") + 1);
            left_bracket = nodeData.indexOf("[");
            right_bracket = nodeData.indexOf("]");
            String impressions_list = nodeData.substring(left_bracket + 1, right_bracket);
            String[] impressions_arr = impressions_list.split("#");

            List<String> impressions = new ArrayList<>();
            Hashtable<String, String> price_map = new Hashtable<>();
            Double average_price = 0.0;
            int average_count = 0;

            nodeData = nodeData.substring(nodeData.indexOf("]") + 1);
            left_bracket = nodeData.indexOf("[");
            right_bracket = nodeData.indexOf("]");
            String prices_list = nodeData.substring(left_bracket + 1, right_bracket);
            String[] prices_arr = prices_list.split("#");

            for (int i = 0; i < impressions_arr.length; i++)
            {
                impressions.add("[" + impressions_arr[i] + "]");
                price_map.put("[" + impressions_arr[i] + "]", prices_arr[i]);
            }

            checkedNodes.clear();
            //Impressions End

            List<String> answers = new ArrayList<>();
            List<String> markov_hotels = new ArrayList<>();
            List<String> seen_hotels = new ArrayList<>();
            if (source_data.length >= 2)
            {
                int markov_limit = 1;
                int markov_count = 0;
                int loop_limit = 0;
                loop_limit = Math.max(loop_limit, 0);
                for (int i = source_data.length - 2; i >= loop_limit; i--)
                {
                    nodeData = source_data[i];
                    left_bracket = nodeData.indexOf("[");
                    right_bracket = nodeData.indexOf("]");
                    String relationship = nodeData.substring(0, left_bracket - 1).split(",")[1];
                    String item = nodeData.substring(left_bracket + 1, right_bracket);
                    if (item.matches(".*[a-zA-Z]+.*"))
                    {
                        nodeData = "[" + relationship + "," + item + "," + region_id + "]";
                        continue;
                    }
                    else
                    {
                        nodeData = "[" + item + "]";
                    }

                    //Add seen hotels to stack
                    if (seen_hotels.indexOf(nodeData) == -1)
                    {
                        seen_hotels.add(nodeData);
                    }

                    if (markov_count >= markov_limit)
                    {
                        continue;
                    }

                    temp = dictionary.get(nodeData);
                    if (temp != null)
                    {
                        MapNodeRecursive(temp, checkedNodes, result, 0, depth, 1.0);
                    }

                    if (price_map.get(nodeData) != null)
                    {
                        Double price = Double.parseDouble(price_map.get(nodeData));
                        average_price += price;
                        average_count++;
                    }

                    markov_count++;
                }
            }
            else
            {
                nodeData = "";
            }

            result.CalculateProbability();
            result.SortProbability();

            for (int j = 0; j < result.items.size(); j++)
            {
                markov_hotels.add(result.items.get(j).id);
            }

            //BiS Start
//            FilterArray(seen_hotels, impressions, true);
//            answers.addAll(seen_hotels);
//
//            //Impressions padding
//            int impression_limit = 4;
//            for (int i = 0; i < impression_limit; i++)
//            {
//                if (impressions.size() > 0)
//                {
//                    answers.add(impressions.get(0));
//                    impressions.remove(0);
//                }
//            }

            impressions_length += impressions.size();

            FilterArray(markov_hotels, impressions, true);
            answers.addAll(markov_hotels);

            markov_length += markov_hotels.size();

            //Get distance from average_price;
//            average_price /= average_count;
//            if (average_price > 0)
//            {
//                List<Double> price_distance = new ArrayList<>();
//                for (int j = 0; j < impressions.size(); j++)
//                {
//                    Double price_j = Double.parseDouble(price_map.get(impressions.get(j)));
//                    Double dist_j = Math.abs(price_j - average_price);
//                    price_distance.add(dist_j);
//                }
//
//                //Sort impressions based on distance from average_price
//                SortArray(impressions, price_distance);
//            }

            answers.addAll(impressions);
            //BiS End


            //Find answer position
            answer_index = GetAnswerIndex(answers, source_id);

            if (answer_index != -1)
            {
                Double correct_val = (1.0 / (answer_index + 1));
                correct += correct_val;
                Double remaining = (1.0 - correct_val);
                incorrect += remaining;
            }
            else
            {
                none++;
            }

            buffer.write(source_id + ";" + prediction_id + ";" + nodeData + ";" + answer_index + ";" + average_price);
            buffer.newLine();

            if (answer_index >= 0)
            {
                track_record.set(answer_index, track_record.get(answer_index) + 1);
            }

            System.out.println(count);
            count++;
        }
        buffer.write("Accuracy : " + (correct / count));
        buffer.newLine();
        buffer.write("None : " + (none / count));
        buffer.newLine();
        buffer.write("Previous : " + (previous / count));
        buffer.newLine();
        buffer.write("Incorrect : " + (incorrect / count));
        buffer.newLine();
        buffer.write("Markov Length : " + (markov_length / count));
        buffer.newLine();
        buffer.write("Impressions Length : " + (impressions_length / count));
        buffer.newLine();
        buffer.write("Track Record : ");
        buffer.newLine();
        Double record_mrr = 0.0;
        for (int i = 0; i < track_record.size(); i++)
        {
            record_mrr += track_record.get(i) * 1 / (i + 1);
            buffer.write("#" + i + " : " + track_record.get(i));
            buffer.newLine();
        }
        buffer.write("Cumulative Track Record : ");
        buffer.newLine();
        Double track_total = 0.0;
        for (int i = 0; i < track_record.size(); i++)
        {
            track_total += track_record.get(i);
            buffer.write("#" + i + " : " + track_total);
            buffer.newLine();
        }
        buffer.write("Record MRR : " + (record_mrr / track_total));
        buffer.newLine();
        buffer.close();
    }

    public static int GetAnswerIndex(List<String> array, String target)
    {
        for (int i = 0; i < array.size(); i++)
        {
            if (array.get(i).equals(target))
            {
                return i;
            }
        }
        return -1;
    }

    public static void FilterArray(List<String> target, List<String> reference, boolean remove)
    {
        for (int i = 0; i < target.size();)
        {
            boolean erase = true;
            for (int j = 0; j < reference.size(); j++)
            {
                if (target.get(i).equals(reference.get(j)))
                {
                    if (remove)
                    {
                        reference.remove(j);
                    }
                    erase = false;
                    break;
                }
            }
            if (erase)
            {
                target.remove(i);
            }
            else
            {
                i++;
            }
        }
    }

    public static void SortArray(List<String> array, List<Double> reference)
    {
        for (int j = 0; j < array.size(); j++)
        {
            for (int k = j; k < array.size(); k++)
            {
                if (reference.get(k) < reference.get(j))
                {
                    String impression_temp = array.get(j);
                    array.set(j, array.get(k));
                    array.set(k, impression_temp);

                    Double price_temp = reference.get(j);
                    reference.set(j, reference.get(k));
                    reference.set(k, price_temp);
                }
            }
        }
    }

    public static void RecalculateAccuracy(String sourceFile, String resultFile) throws IOException
    {
        BufferedReader source = new BufferedReader(new FileReader(sourceFile));
        FileWriter writer = new FileWriter(resultFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        String source_line;
        String[] source_data;
        Double count = 0.0, correct = 0.0, none = 0.0, incorrect = 0.0;
        while ((source_line = source.readLine()) != null)
        {
            if (source_line.contains(";"))
            {
                source_data = source_line.split(";");
                if (source_data[0].contains("["))
                {
                    if (source_data[0].equals(source_data[1]))
                    {
                        //System.out.println(source_data[0] + " " + source_data[1]);
                        correct++;
                    }
                    else
                    {
                        if (source_data[1].equals("none"))
                        {
                            none++;
                        }
                        else
                        {
                            incorrect++;
                        }
                    }
                    buffer.write(source_line);
                    buffer.newLine();
                    count++;
                }
            }
        }
        buffer.write("Accuracy : " + (correct / count));
        buffer.newLine();
        buffer.write("None : " + (none / count));
        buffer.newLine();
        buffer.write("Incorrect : " + (incorrect / count));
        buffer.newLine();
        buffer.close();
    }

    public static void ItemTreeMap(String inFile, String outFile) throws IOException
    {
        dictionary.clear();

        String[] data, session;
        String regionID;
        String line;
        Node temp, existing;
        int count = 0;
        int window_limit = 1;

        System.out.println("Loading to list..");
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        List<String> multi_order = new ArrayList<>();
        while ((line = reader.readLine()) != null)
        {
            data = line.split(";");
            data = data[1].split("\\|");
            regionID = data[0];

            int window = 1;
            String[] temp_arr = data[1].split("->");

            //System.out.println(line);
            while(window <= window_limit)
            {
                String item_arr = regionID + "|";
                boolean save = false;
                for (int i = 0; i <= temp_arr.length - window; i++)
                {
                    save = true;
                    String relationship = "action";
                    String hotel_id = window + step_delimiter + relationship + "[";
                    int hotel_count = 0;
                    for (int j = 0; j < window; j++)
                    {
                        String sub_relationship = temp_arr[i + j].substring(temp_arr[i + j].indexOf(",") + 1, temp_arr[i + j].indexOf("[") - 1);
                        String clean_id = temp_arr[i + j].substring(temp_arr[i + j].indexOf("[") + 1, temp_arr[i + j].indexOf("]"));
                        if ((clean_id.equals(regionID) && sub_relationship.equals("search for destination")))
                        {
                            continue;
                        }
                        hotel_count++;
                        String final_id = (clean_id.matches(".*[a-zA-Z]+.*")) ? (sub_relationship + "," + clean_id + "," + regionID) : clean_id;
                        hotel_id += final_id;
                        if (j + 1 < window)
                        {
                            hotel_id += ";";
                        }
                    }
                    hotel_id += "]";
                    if (hotel_count < window)
                    {
                        continue;
                    }
                    //System.out.println(hotel_id);
                    item_arr += hotel_id;
                    if (i + 1 <= temp_arr.length - window)
                    {
                        item_arr += "->";
                    }
                }
                if (save)
                {
                    //System.out.println("Window #" + window + ": ");
                    //System.out.println(item_arr);
                    multi_order.add(item_arr);
                }
                window++;
            }
            count++;
            System.out.println(count);
        }
        reader.close();

        for (int j = 0; j < multi_order.size(); j++)
        {
            System.out.println(j);
            data = multi_order.get(j).split("\\|");
            regionID = data[0];
            Node region_node = dictionary.get(regionID);

            if (region_node == null)
            {
                dictionary.put(regionID, new Node(regionID));
            }

            if (data.length != 2)
            {
                continue;
            }

            //System.out.println(data[1]);
            data = data[1].split("->");
            temp = dictionary.get(regionID);

            //System.out.println(multi_order.get(j));
            for (int i = 0; i < data.length; i++)
            {
                //System.out.println(data[i]);
                session = data[i].split(step_delimiter);
                int window = Integer.parseInt(session[0]);
                int left_bracket = session[1].indexOf("[");
                int right_bracket = session[1].indexOf("]");
                String relationship = session[1].substring(0, left_bracket - 1);
                String item = session[1].substring(left_bracket + 1, right_bracket);
                String item_combined = "[" + item + "]";
                String first_item = "[" + item.split(item_delimiter)[window - 1] + "]";

                if (!(item.equals(regionID) && relationship.equals("search for destination")))
                {
                    existing = dictionary.get(first_item);
                    if (existing == null)
                    {
                        existing = new Node(first_item);
                        dictionary.put(first_item, existing);
                    }
                    temp.Register(existing, first_item, 1.0);

                    Node current_node = dictionary.get(item_combined);
                    if (current_node == null)
                    {
                        current_node = new Node(item_combined);
                        dictionary.put(item_combined, current_node);
                    }
                    temp = current_node;
                }
            }
        }

        System.out.println("Mapping graph..");

        System.out.println("Saving graph to file..");
        SaveGraphToFile(outFile);
    }

    public static void LoadFile(String fileName) throws IOException
    {
        content = new ArrayList<String>();
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        while ((line = reader.readLine()) != null) {
            content.add(line);
        }
        reader.close();
    }

    public static void SaveGraphToFile(String fileName) throws IOException
    {
        FileWriter writer = new FileWriter(fileName);
        BufferedWriter buffer = new BufferedWriter(writer);
        Set<String> keys = dictionary.keySet();

        Node node;
        Double total, average, stdDev;
        for(String key: keys)
        {
            node = dictionary.get(key);
            total = 0.0;
            stdDev = 0.0;

            node.CalculateProbability();
            node.SortProbability();
            for (int i = 0; i < node.items.size(); i++)
            {
                total += node.probability.get(i);
            }

            average = (node.items.size() > 0) ? total / (double)node.items.size() : 0.0;
            for (int i = 0; i < node.items.size(); i++)
            {
                stdDev += Math.pow((node.probability.get(i) - average), 2.0);
            }
            stdDev = (node.items.size() > 0) ? stdDev / node.items.size() : 0;
            stdDev = (Double)Math.pow(stdDev, 0.5);

            buffer.write("#" + node.id + "#");
            buffer.newLine();
            buffer.write("Total : " + total);
            buffer.newLine();
            buffer.write("Average + Std Dev : " + (average + stdDev));
            buffer.newLine();
            for (int i = 0; i < node.items.size(); i++)
            {
                buffer.write(node.items.get(i).id + ";" + node.probability.get(i));
                buffer.newLine();
            }
        }
        buffer.close();
    }

    public static void SessionSort(String fileName, String fileName2, int boundaryTop, int boundaryBottom) throws IOException
    {
        FileWriter writer = new FileWriter(fileName);
        BufferedWriter buffer = new BufferedWriter(writer);

        FileWriter writer2 = new FileWriter(fileName2);
        BufferedWriter buffer2 = new BufferedWriter(writer2);

        String[] arr, point, bites;
        List<String> sessions = new ArrayList<String>();
        List<Integer> index = new ArrayList<Integer>();
        int tempInt, step;
        String tempStr, newContent, lastSeen;

        for (int i = 0; i < content.size(); i++)
        {
            arr = content.get(i).split("\\|");
            sessions.clear();
            index.clear();

            point = arr[1].split("->");
            boolean proceed = true;
            for (int j = 0; j < point.length; j++) {
                if (!point[j].equals("")) {
                    bites = point[j].split(comma_ex_quote_delimiter);
                    try {
                        index.add(Integer.parseInt(bites[0]));
                        sessions.add(bites[1]);
                    } catch (Exception e) {
                        proceed = false;
                        break;
                    }
                }
            }

            if (!proceed) {
                continue;
            }

            for (int j = 0; j < index.size(); j++)
            {
                for (int k = j; k < index.size(); k++)
                {
                    if (index.get(k) < index.get(j))
                    {
                        tempInt = index.get(j);
                        tempStr = sessions.get(j);

                        index.set(j, index.get(k));
                        index.set(k, tempInt);

                        sessions.set(j, sessions.get(k));
                        sessions.set(k, tempStr);
                    }
                }
            }

            newContent = "";
            lastSeen = "";
            step = 1;

            for (int j = 0; j < index.size(); j++)
            {
                if (!sessions.get(j).equals(lastSeen))
                {
                    newContent += step + "," + sessions.get(j);
                    newContent += "->";
                    step++;
                    lastSeen = sessions.get(j);
                }
            }
            newContent = newContent.substring(0, newContent.length() - 2);

            if (i >= boundaryTop && i < boundaryBottom)
            {
                buffer2.write(arr[0] + "|" + newContent);
                buffer2.newLine();
            }
            else
            {
                buffer.write(arr[0] + "|" + newContent);
                buffer.newLine();
            }
        }
        buffer.close();
        buffer2.close();

        content.clear();
    }

    public static void SessionTestFilter(String inFile, String outFile) throws IOException
    {
        String[] data;
        String line, session, temp;
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        FileWriter writer = new FileWriter(outFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        while ((line = reader.readLine()) != null)
        {
            data = line.split("\\|");
            session = data[0];
            data = data[1].split("->");
            for (int i = 0; i < data.length; i++)
            {
                if (data[i].contains("clickout"))
                {
                    temp = session + "|";
                    for (int j = 0; j <= i; j++)
                    {
                        temp += data[j] + "->";
                    }
                    temp = temp.substring(0, temp.length() - 2);
                    buffer.write(temp);
                    buffer.newLine();
                }
            }
        }
        buffer.close();
    }

    public static void SessionTestHide(String inFile, String outFile) throws IOException
    {
        String[] data;
        String line, session, temp;
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        FileWriter writer = new FileWriter(outFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        while ((line = reader.readLine()) != null)
        {
            data = line.split("\\|");
            session = data[0];
            temp = session + "|";
            data = data[1].split("->");
            for (int i = 0; i < data.length - 1; i++)
            {
                temp += data[i] + "->";
            }
            if (data.length > 1)
            {
                temp = temp.substring(0, temp.length() - 2);
            }
            buffer.write(temp);
            buffer.newLine();
        }
        buffer.close();
    }

    public static void main(String[] args) throws Exception
    {
        long start = System.currentTimeMillis();

        Configuration conf = new Configuration();
        conf.set(TextOutputFormat.SEPERATOR, "|");
        FileSystem hdfs = FileSystem.get(conf);
        Job job;

        /*job = Job.getInstance(conf, "Session Action List");

        job.setJarByClass(MarkovChain.class);
        job.setMapperClass(SessionActionMapper.class);
        job.setCombinerClass(SessionActionReducer.class);
        job.setReducerClass(SessionActionReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("trivago_train/train.csv"));
        if (hdfs.exists(new Path("trivago_train_session")))
        {
            hdfs.delete(new Path("trivago_train_session"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_train_session"));
        job.waitForCompletion(true);*/

        /*job = Job.getInstance(conf, "Price Mapper");

        job.setJarByClass(MarkovChain.class);
        job.setMapperClass(PriceMapper.class);
        job.setCombinerClass(PriceReducer.class);
        job.setReducerClass(PriceReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("trivago_train/train.csv"));
        if (hdfs.exists(new Path("trivago_price")))
        {
            hdfs.delete(new Path("trivago_price"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_price"));
        job.waitForCompletion(true);*/

        /*job = Job.getInstance(conf, "Impression Mapper");

        job.setJarByClass(MarkovChain.class);
        job.setMapperClass(ImpressionMapper.class);
        job.setCombinerClass(ImpressionReducer.class);
        job.setReducerClass(ImpressionReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("trivago_train/train.csv"));
        if (hdfs.exists(new Path("trivago_impression")))
        {
            hdfs.delete(new Path("trivago_impression"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_impression"));
        job.waitForCompletion(true);*/

        Double boundaryTop = 0.9;
        Double boundaryBottom = 1.0;
        Double boundaryLimit = 1.0;
        int fileIndex = 145;

        String session_train, session_test, session_probability, session_test_filtered;
        String session_test_filtered_hidden, session_prediction, session_accuracy;

        while (boundaryBottom <= boundaryLimit)
        {
            LoadFile("trivago_train_session/part-r-00000");
            System.out.println("Iteration : " + fileIndex);

            session_train = "trivago_session/session_train" + fileIndex + ".txt";
            session_test = "trivago_session/session_test" + fileIndex + ".txt";
            session_probability = "trivago_session/probability" + fileIndex + ".txt";
            session_test_filtered = "trivago_session/session_test_filtered" + fileIndex + ".txt";
            session_test_filtered_hidden = "trivago_session/session_test_filtered_hidden" + fileIndex + ".txt";
            session_prediction = "trivago_session/prediction" + fileIndex + ".txt";
            session_accuracy = "trivago_session/accuracy" + fileIndex + ".txt";

            SessionSort(session_train, session_test, (int)(content.size() * boundaryTop), (int)(content.size() * boundaryBottom));
            ItemTreeMap(session_train, session_probability);
            SessionTestFilter(session_test, session_test_filtered);
            SessionTestHide(session_test_filtered, session_test_filtered_hidden);
            CalculateAccuracy(session_test_filtered, session_accuracy);
            boundaryTop += 0.1f;
            boundaryBottom += 0.1f;
            fileIndex++;
            RecalculateAccuracy("trivago_session/accuracy" + (fileIndex - 1) + ".txt", "trivago_session/accuracy" + (fileIndex - 1) + "_recalculated.txt");
        }
        long end = System.currentTimeMillis();
        float sec = (end - start) / 1000F;
        System.out.println(sec + " seconds");
    }
}
